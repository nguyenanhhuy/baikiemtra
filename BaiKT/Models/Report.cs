﻿using System;
using System.Collections.Generic;

namespace BaiKT.Models;

public partial class Report
{
    public int ReportId { get; set; }

    public int? AccountId { get; set; }

    public int? LogsId { get; set; }

    public int? TransactionalId { get; set; }

    public string? Reportname { get; set; }

    public DateOnly? Reportdate { get; set; }

    public virtual Account? Account { get; set; }

    public virtual Log? Logs { get; set; }

    public virtual Transaction? Transactional { get; set; }
}

﻿using System;
using System.Collections.Generic;

namespace BaiKT.Models;

public partial class Account
{
    public int AccountId { get; set; }

    public int? CustomerId { get; set; }

    public string? Accountname { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual ICollection<Report> Reports { get; set; } = new List<Report>();
}
